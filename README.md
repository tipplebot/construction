## tipplebot / construction

This houses the documentation for building your own tipplebot and all details of parts, including source files for 3D printed parts.

## Parts list
** Note this is a work-in-progress parts list (and entire project)
- Main Motor: Nema 23 Stepper Motor 2.8A 178.5oz.in/1.26Nm ([Amazon](https://www.amazon.co.uk/gp/product/B071CN9VCW/ref=oh_aui_detailpage_o04_s00?ie=UTF8&psc=1))
